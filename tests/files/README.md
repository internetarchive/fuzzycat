# Matcher Test Files

The goal here is to have a mostly language-independent test cases for matching.

Each subdirectory corresponds to a test function and contains examples for it.
